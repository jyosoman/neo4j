/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include<storage/BlockStorage.hh>
#include <gtest/gtest.h>

using namespace std;

// A new one of these is created for each test

class BlockStorageTest : public testing::Test {

    unordered_map<int, vector<int>> genGraph(int n, int d) {
        unordered_map<int, vector<int>> graph;
        for (int i = 0; i < n; i++) {
            vector<int> edges;
            for (int j = 0; j < d; j++) {
                edges.push_back(rand() % n);
            }
            graph[i] = edges;
        }
        return graph;
    }


public:
    unordered_map<int, vector<int>> graph;
    BlockStorage* elh;

    virtual void SetUp() {
        graph = genGraph(100, 10);
        elh = new BlockStorage(100, 1000, graph, 0);
    }

    virtual void TearDown() {
        delete elh;
    }
};

TEST_F(BlockStorageTest, testBlockResults) {

    for (unsigned int node = 0; node < graph.size(); node++) {
        vector<int> y = graph[node];
        auto x = elh->getEdges(node);
        ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

        for (unsigned int i = 0; i < x.size(); ++i) {
            EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
        }
    }
}

TEST_F(BlockStorageTest, testBlockDeleteResults) {
    elh->deleteNode(0);
    unsigned int node = 0;
    vector<int> y = graph[node];
    auto x = elh->getEdges(node);
    ASSERT_NE(x.size(), y.size()) << "Vectors x and y are of unequal length";

    for (unsigned int i = 0; i < x.size(); ++i) {
        EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
    }
    for (node = 1; node < graph.size(); node++) {
        vector<int> y = graph[node];
        auto x = elh->getEdges(node);
        ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

        for (unsigned int i = 0; i < x.size(); ++i) {
            EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
        }
    }
}
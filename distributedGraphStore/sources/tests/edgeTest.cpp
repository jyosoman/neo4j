/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <gtest/gtest.h>
#include<storage/edgeHandles.hh>
 

// A new one of these is created for each test
class EdgeTest : public testing::Test
{
public:
    EdgeListHead* elh;
 
  virtual void SetUp()
  {
      elh=new EdgeListHead(0,0);
      elh->addEdge(1,0);
      elh->addEdge(2,0);
  }
 
  virtual void TearDown()
  {
      delete elh;
  }
};
 
TEST_F(EdgeTest, testElementZeroIsOne)
{
    vector<int> y={1,2};
    
  auto x= elh->getNeighbours(0);
  ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

for (unsigned int i = 0; i < x.size(); ++i) {
  EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
}
}

//TEST_F(EdgeTest, testElementOneIsTwo)
//{
//  EXPECT_EQ(2,);
//}
 
TEST_F(EdgeTest, testSizeIsTwo)
{
  EXPECT_EQ((unsigned int)2,elh->getNeighbours(0).size());
}





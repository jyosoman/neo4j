/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


#include<storage/BlockInterface.h>
#include <gtest/gtest.h>
#include<algorithm>

using namespace std;

// A new one of these is created for each test

class BlockInterfaceTest : public testing::Test {

    unordered_map<int, vector<int>> genGraph(int n, int d) {
        unordered_map<int, vector<int>> graph;
        for (int i = 0; i < n; i++) {
            vector<int> edges;
            for (int j = 0; j < d; j++) {
                edges.push_back(rand() % n);
            }
            graph[i] = edges;
        }
        return graph;
    }


public:
    unordered_map<int, vector<int>> graph, graph2;
    BlockInterface elh;

    virtual void SetUp() {
        int n1=100,n2=150;
        int d=10;
        graph = genGraph(n1, d);
        graph2=genGraph(n2,d);
        elh.addBlock(n1, n1*d, graph,0);
        elh.addBlock(n2,n2*d,graph2,0);
        for(auto x:graph2){
            graph[x.first].insert(graph[x.first].end(),x.second.begin(),x.second.end());
        }
    }

    virtual void TearDown() {
       
    }
};

TEST_F(BlockInterfaceTest, testBlockBasicResults) {

    for (unsigned int node = 0; node < graph.size(); node++) {
        vector<int> y = graph[node];
        sort(y.begin(),y.end());
        auto x = elh.getEdges(node);
        sort(x.begin(),x.end());
        ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

        for (unsigned int i = 0; i < y.size(); i++) {
            EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
        }
    }
}
TEST_F(BlockInterfaceTest, testBlockMergeResults) {
    elh.mergeBlocks(0,1);
    for (unsigned int node = 0; node < graph.size(); node++) {
        vector<int> y = graph[node];
        sort(y.begin(),y.end());
        auto x = elh.getEdges(node);
        sort(x.begin(),x.end());
        ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length "<<node;

        for (unsigned int i = 0; i < y.size(); ++i) {
            EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
        }
    }
}
TEST_F(BlockInterfaceTest, testBlockDeleteResults) {
    
    unsigned int node = 0;
    
    graph[node].clear();
    
    elh.deleteNode(node);
    
   
    for (node = 0; node < graph.size(); node++) {
        vector<int> y = graph[node];
        auto x = elh.getEdges(node);
        ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

        for (unsigned int i = 0; i < x.size(); ++i) {
            EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
        }
    }
}
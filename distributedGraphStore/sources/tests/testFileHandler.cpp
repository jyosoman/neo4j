#include<iostream>
#include<cstdio>
#include"fileHandler.hh"
using namespace std;
void testOpeningFile(int n){
    string file="testfh.txt";
    FileAllocator<int> fmm(file,n);
    int *arr=(int*)fmm.allocate(n*sizeof(int));
#pragma omp parallel num_threads(4) 
    {
#pragma omp for

        for(int i=0;i<n;i++){
            arr[i]=i;
        }
    }

}
void testReOpeningFile(){
    string file="testfh.txt";
    FileAllocator<int> fmm(file);
    int **arr=(int**)fmm.getStart();
    cout<<"Arr:"<<*arr<<endl;
    for(int i=0;i<100;i++){
        cout<<i<<" "<<arr[0][i]<<endl;
    }
}

int main(int argc, char* argv[]){
    int n=1000000;
    if(argc>1){
        n=atoi(argv[1]);
    }
    testOpeningFile(n);
    /* testReOpeningFile(); */
}

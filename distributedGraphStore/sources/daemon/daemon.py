import server
import ConfigParser
import argparse
import json

def loadCLArgs():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--file',action="store", default="config.ini",help='Config file')
    parser.parse_args()
    return parser.parse_args()
def loadConfigFile(filename):
    Config = ConfigParser.ConfigParser()
    Config.read(filename)
    return Config
if __name__ == '__main__':
    parser=loadCLArgs()
    config=loadConfigFile(parser.file)
    server=server.Server(config)
    server.getData()


#ifndef __FILEHANDLER__HH
#define __FILEHANDLER__HH
#include<mutex>
#include<string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include<memory>
#include<cstring>
#include<iostream>
#include<unordered_map>
#include<list>
#include<vector>

using namespace std;

class FileHandler {
    
protected:
    char* mmapBegin;
    int fd;
    long long int currIndex;
    int filesize;
    string filename;
    string metaDataFile;

    unordered_map<size_t, list<long long int>> deletedList;

    bool fileExists(string fName);

    void stretchFile();

    mutex mtx;

public:

    long long int getNewBlock(size_t dataLen);

    bool releaseBlock(long long int index, size_t size) {
        if (!checkIndex(index)) {
            return false;
        }
        memset((mmapBegin + index), 0, size);
        mtx.lock();
        deletedList[size].push_back(size);
        mtx.unlock();
        return true;
    }

    FileHandler(string);

    FileHandler(string, int size);

    long long int appendToFile(char* data, long long int dataLen);

    void deepCopy(long long int index, int dataLength, char**data) {
        memcpy(*data, mmapBegin + index, dataLength);
    }

    bool writeData(long long int index, const char* data, int length);

    //Do not use this unless you really need to. 

    bool writeData(const char* data, int length) {
        return writeData(getNewBlock(length), data, length);
    }

    bool writeData(long long int index, const char* data, int length, int subIndex);

    bool checkIndex(long long int index);

    vector<char> readData(long long int index, int subIndex, int length);

    ~FileHandler();

};

//overloading allocator is a nightmare. Will take this up when sleep is less dramatic. 
//Adios allocator. 
//
//Overloading operations to a memory allocator, so that any access is done to the lowest layer. 
//This would be a bit complex to implement, so lets not do that now. 

/* template <class T> class FileAllocator:public FileHandler,public allocator<T>{ */
/*     public: */
/*         FileAllocator<T>(string fname):FileHandler(fname){ */
/*         } */
/*         FileAllocator<T>(string fname,int fsize):FileHandler(fname,fsize){ */
/*         } */
/*         void* allocate(size_t num, const void* hint=0){ */
/*             return(void*) (getBlock(num)+mmapBegin); */
/*         } */
/*         void* getStart(){ */
/*             return (void*)mmapBegin; */
/*         } */

/*         long long int allocate(size_t sz){ */
/*             return getBlock(sz); */
/*         } */

/* }; */

/* template <class T> class FileMemoryManager:public FileAllocator<T>{ */
/*     public: */
/*         FileMemoryManager<T>(string fname):FileAllocator<T>(fname){} */
/*         void * operator new(size_t size) */
/*         { */
/*             void * p = (void*)FileAllocator<T>::allocate(size,NULL); */
/*             return p; */
/*         } */
/*         static void operator delete(void* block){} */
/* }; */
#endif 

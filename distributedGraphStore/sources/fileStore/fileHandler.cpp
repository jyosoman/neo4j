#include"fileHandler.hh"
#include<fstream>
#include<iostream>
#include<mutex>
using namespace std;

FileHandler::FileHandler(string fname){
    filename=fname;
    metaDataFile=filename+".meta";
//    int i;
//    int result;

    if(!fileExists(metaDataFile)){
        currIndex=0;
        filesize=1000000*sizeof(int);
    }else{
        ifstream inFile;
        inFile.open(metaDataFile);
        if(inFile>>currIndex){
            if(inFile>>filesize){
            }else{
                filesize=1000000*sizeof(int);
            }
        }else{
            currIndex=0;
            filesize=1000000*sizeof(int);
        }
    }
    fd = open(filename.c_str(), O_RDWR | O_CREAT, (mode_t)0600);

    if (fd == -1) {
        cout<<fname<<endl;
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }
    if (lseek(fd, filesize-1, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
    }   

    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
    }

    mmapBegin =(char*) mmap(0, filesize, PROT_READ | PROT_WRITE, MAP_SHARED|MAP_NORESERVE, fd, 0);
    msync(mmapBegin,filesize, MS_SYNC);
    if (mmapBegin == MAP_FAILED) {
        close(fd);
        perror("Error mmapping the file");
        exit(EXIT_FAILURE);
    }

}

FileHandler::FileHandler(string fname, int fsize){
    filename=fname;
    metaDataFile=filename+".meta";
    int i;
    int result;
    filesize=(fsize+8)*sizeof(int);
    if(!fileExists(metaDataFile)){
        currIndex=0;
    }else{
        ifstream inFile;
        inFile.open(metaDataFile);
        if(inFile>>currIndex){
        }else{
            currIndex=0;
        }
    }
    fd = open(filename.c_str(), O_RDWR | O_CREAT, (mode_t)0600);

    if (fd == -1) {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }
    if (lseek(fd, filesize-1, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
    }   

    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
    }

    mmapBegin =(char*) mmap(0, filesize, PROT_READ | PROT_WRITE, MAP_SHARED|MAP_NORESERVE, fd, 0);
    msync(mmapBegin,filesize, MS_SYNC);
    if (mmapBegin == MAP_FAILED) {
        close(fd);
        perror("Error mmapping the file");
        exit(EXIT_FAILURE);
    }
}

long long int FileHandler::appendToFile(char* data, long long int dataLen){
    char * cstr = data;
    dataLen=(dataLen+8)-dataLen%8;
    long long int cIndex=dataLen;
    __atomic_fetch_add (&currIndex, cIndex, __ATOMIC_ACQ_REL);
    for(int i=0;i<dataLen;i++){
        mmapBegin[cIndex++]=cstr[i];
    }
    msync(mmapBegin,filesize, MS_SYNC);
    return cIndex;
}

bool FileHandler::fileExists(string fName){
    struct stat buffer;   
    return (stat (fName.c_str(), &buffer) == 0); 
}

void FileHandler::stretchFile(){
    msync(mmapBegin,filesize, MS_SYNC);
    if (lseek(fd, filesize-1, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
    }   

    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
    }   
    mmapBegin =(char*) mmap(0, filesize+filesize, PROT_READ | PROT_WRITE, MAP_SHARED|MAP_NORESERVE, fd, 0);
    filesize+=filesize;
}

bool FileHandler::checkIndex(long long int index){
    if(index<currIndex)
        return true;
    return false;
}

vector<char> FileHandler::readData(long long int index, int subIndex, int length){
    vector<char> result(length);
    if((length+subIndex)>16||(!checkIndex(index))||(index<currIndex)){
        result.resize(0);
        return result;
    }

    for(int i=0;i<length;i++){
        result[i]=mmapBegin[index+subIndex+i];
    }
    return result;
}

bool FileHandler::writeData(long long int index,const char* data, int length){
    if(!checkIndex(index)){
        return false;
    }
    if(length>16){
        return false;
    }
    for(int i=0;i<length;i++){
        mmapBegin[index+i]=data[i];
    }
    return true;
}

bool FileHandler::writeData(long long int index,const char* data, int length, int subIndex){
    if(!checkIndex(index)){
        return false;
    }

    if((length+subIndex)>16){
        return false;
    }
    for(int i=0;i<length;i++){
        mmapBegin[index+subIndex+i]=data[i];
    }
    return true;
}

long long int FileHandler::getNewBlock(size_t dataLen){
    dataLen=(dataLen+8)-dataLen%8;
    long long int cIndex=dataLen;
    long long int prev=currIndex;
    if(deletedList[dataLen].size()>0){
        mtx.lock();
        if(deletedList[dataLen].size()>0){
            cIndex=deletedList[dataLen].front();
            deletedList[dataLen].pop_front();
            mtx.unlock();
            return cIndex;
        }
        mtx.unlock();
    }
    cIndex=__atomic_fetch_add(&currIndex, cIndex, __ATOMIC_ACQ_REL);
    if(currIndex>(filesize-1024)&&(currIndex-dataLen)<=(filesize-1024)){
        stretchFile();
    }
    return cIndex;
}

FileHandler::~FileHandler(){
    ofstream metafile;
    metafile.open (metaDataFile, ios::out | ios::trunc);
    metafile<<currIndex<<endl;
    metafile<<filesize<<endl;
    msync(mmapBegin,filesize, MS_SYNC);
    if (munmap(mmapBegin, filesize) == -1) {
        perror("Error un-mmapping the file");
    }
    close(fd);
}


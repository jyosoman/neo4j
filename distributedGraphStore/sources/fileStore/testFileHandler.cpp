#include<iostream>
#include<cstdio>
#include"mmap_allocator.h"
#include<string>
using namespace std;
int main(){
    string file="testfh.txt";
    mmap_allocator_namespace::mmap_allocator<int> fmm(file,mmap_allocator_namespace::READ_WRITE_SHARED,0);
    int *arr=(int*)fmm.allocate(100);
    cout<<arr<<endl;
    for(int i=0;i<100;i++){
        arr[i]=i;
    }
}

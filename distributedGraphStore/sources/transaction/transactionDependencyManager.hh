#include"transaction.hh"
#include<map>
#include<set>
//This would have to be read from a config file. 
//In case the number of clients are larger, the responsibility is on the front end to malong long intain connections. 
//Ideally, not looking at over subscribing by more than 2X on a machine. 
//then the front end would have to read the data and pass it to the worker threads, so that no client is held waiting, but the     

using namespace std;
class TransactionGraph{
    
    long long int maxInFlightTransactions;
    vector<vector<long long int>>connectionArray;
    public:
    TransactionGraph(long long int maxift){
        maxInFlightTransactions=maxift;
        connectionArray=vector<vector<long long int>>(maxift,vector<long long int>(maxift,0));
    }
    ~TransactionGraph(){
    }
    long long int getMaxInFlightTransactions(){
        return maxInFlightTransactions;
    }
    void clearRow(long long int id){
        for(long long int i=0;i<maxInFlightTransactions;i++){
            connectionArray[id][i]=0;
        }
    }
    void clearColoumn(long long int id){
         for(long long int i=0;i<maxInFlightTransactions;i++){
            connectionArray[i][id]=0;
        }
    }

    vector<long long int> checkLoop(long long int id){
        bool seen[maxInFlightTransactions]={false};
        vector<long long int> loop;
        vector<long long int> currIndex;
        loop.push_back(id);
        currIndex.push_back(0);
        while(loop.size()!=0){
            long long int curr=loop.back();
            long long int index=currIndex.back();
            seen[curr]=true;
            for(auto it=index;it<maxInFlightTransactions;it++){
                if(connectionArray[curr][it]!=0){
                    if(!seen[it]){
                        currIndex[currIndex.size()-1]=it;
                        loop.push_back(it);
                        currIndex.push_back(0);
                        break;
                    }else{
                        if(it==id){
                            return loop;
                        }
                    }
                }
            }
            if(curr==loop.back()){
                loop.pop_back();
                currIndex.pop_back();
            }
        }
    }
    
    void addDependency(long long int from, long long int to){
        connectionArray[from][to]=1;
    }

    void removeDependency(long long int from, long long int to){
        connectionArray[from][to]=0;
    }

    void clearTransaction(long long int id){
        clearRow(id); clearColoumn(id);
    }
};

class TransactionDependencyManager{
    //Still need to make the different operations atomic.
    //Add more atomics maybe;
    //
    //Each transaction gets its own internal id here. 
    //

    TransactionGraph tg;
    map<long long int,long long int> trIdToThreadIdMap;
    set<long long int> availableIds;

    public:
    TransactionDependencyManager(long long int mift):tg(mift){
        for(long long int i=0;i<mift;i++)
            availableIds.insert(i);
    }
    bool addTransaction(long long int trid){
        if(availableIds.size()==0){
            return false;
        }
        trIdToThreadIdMap[trid]=*(availableIds.begin());
        availableIds.erase(availableIds.begin());
        return true;
    }
    bool addDependency(long long int tid1,long long int tid2){
        long long int intTid1=trIdToThreadIdMap[tid1],intTid2=trIdToThreadIdMap[tid2];
        tg.addDependency(intTid1, intTid2);
        if(tg.checkLoop(intTid1).size()>0){
            return false;
        }
        tg.removeDependency(intTid1,intTid2);
    }
    void removeTransaction(long long int tid){
        long long int intTid=trIdToThreadIdMap[tid];
        tg.clearTransaction(intTid);
        availableIds.insert(intTid);
        trIdToThreadIdMap.erase(tid);
    }
};





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TransactionFactory.hh
 * Author: jyothish
 *
 * Created on 13 April 2018, 12:26
 */

#ifndef TRANSACTIONFACTORY_HH
#define TRANSACTIONFACTORY_HH

#include"transaction/transaction.hh"
#include<unordered_map>
#include <typeindex> 
#include<cassert>
#include<string>
#include<sstream>
class TransactionFactory {
    unordered_map<string, char> opMapper;

    vector<string> split(const string &s, char delim) {
        stringstream ss(s);
        string item;
        vector<string> tokens;
        while (getline(ss, item, delim)) {
            if (item.size() > 0)
                tokens.push_back(item);
        }
        return tokens;
    }
    unordered_set<string> validOps;
    
    Transaction* createTransaction(vector<string> transactionOperations) {
        Transaction* t = new Transaction();
        for (auto x : transactionOperations) {
            vector<string> tokens = split(x, ' ');
            if (tokens.size() > 1) {
                string op(1, '\0');
                {
                    if (opMapper.find(tokens[0]) != opMapper.end()) {
                        op[0] +=opMapper[tokens[0]];
                    }
                    if (opMapper.find(tokens[1]) != opMapper.end()) {
                        op[0] +=opMapper[tokens[1]];
                    }                    
                }
                for(int i=2;i<tokens.size();i++){
                    op+=tokens[i];
                }
                t->operations.push_back(op);
            }
        }
    }

    string createOperation(char op, long long int n1, long long int n2){
        string s(18,'\0');
        s[0]=op;
        
        string a=to_string(n1);

        string b=to_string(n2);

        for(int i=0;i<a.length();i++){
            s[1+i]=a[i];
        }
        
        for(int i=0;i<b.length();i++){
            s[9+i]=b[i];
        }

        //cout<<s<<endl;
        return s;
    }
        
public:

    TransactionFactory() {
        opMapper["Read"] = 0;
        opMapper["Write"] = 1;
        opMapper["Delete"] = 2;
        
        opMapper["Node"] = 0;
        opMapper["Edge"] = 4;
        opMapper["Neighbour"] = 8;
    }



    Transaction* createRandomWriteTransaction(int maxNodes, int maxEdges){
        int nodeNum=maxNodes;
        int edgeNum=maxEdges;
        Transaction* t=new Transaction();
        for(int i=1;i<=nodeNum;i++){
            t->operations.push_back(createOperation(opMapper["Write"]+opMapper["Node"],i,0));
        }
        for(int i=0;i<edgeNum;i++){
             t->operations.push_back(createOperation(opMapper["Write"]+opMapper["Edge"],1+(rand()%nodeNum),1+(rand()%nodeNum)));
        }
        return t;        
    }
    
    Transaction* createRandomReadWriteTransaction(int written, int newNodes, int newEdges){
       Transaction* t=new Transaction();  
        for(int i=0;i<newNodes;i++){
             t->operations.push_back(createOperation(opMapper["Write"]+opMapper["Node"],i+written+1,0));
        }
        for(int i=0;i<newEdges;i++){
             t->operations.push_back(createOperation(opMapper["Write"]+opMapper["Edge"],1+(rand()%(written+newNodes)),1+(rand()%(written+newNodes))));
        }
        for(int i=0;i<newEdges;i++){
             t->operations.push_back(createOperation(opMapper["Read"]+opMapper["Neighbour"],1+(rand()%(written+newNodes)),0));
        }
       return t;

    }

};
#endif /* TRANSACTIONFACTORY_HH */


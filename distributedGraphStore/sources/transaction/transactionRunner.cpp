#include"transactionRunner.hh"

lli TransactionRunner::addEdge(lli from, long long int to) {
    lli fid, tid;
    fid = getId(from);
    tid = getId(to);
    edgeWriteSet[fid].push_back(tid);
    return 1;
}

lli TransactionRunner::addNode(lli id) {
    cout << "Add node: " << id << endl;
    nodeWriteSet.insert(id);
    return 1;
}

lli TransactionRunner::deleteNode(lli id) {
    nodeDeleteSet.insert(db->getNodeId(id));
}

lli TransactionRunner::deleteEdge(lli from, lli to) {
    edgeWriteSet.insert(make_pair(getId(from), getId(to)));
    return 1;
}

vector<lli> TransactionRunner::readNeighbour(lli id) {
    lli lid = getId(id);
    edgeReadSet.insert(lid);
    vector<lli> result;
    if (lid > 0) {
        result = db->getNeighbours(id, getTransactionId());
        if (edgeWriteSet.find(id) != edgeWriteSet.end()) {
            for (auto x : edgeWriteSet[id]) {
                result.push_back(x);
            }
        }
        for (unsigned int x = 0; x < result.size(); x++) {
            if (deletedNode(result[x])) {
                result[x] = 0;
            }
        }
    }
    return result;
}

/*
 *  Each statement will be of the format: 
 *  1 byte: type of operation: 
 *      2 bit: Operation type: Read, Write, Delete 0,1,2
 *      2 bit: Operating on: Node, Edge, EdgeList 0,1,2 
 *      1 bit: Property/ID  1,0
 *      1 bit: Conditional/Unconditional 0,1
 *      1 bit: Independent/Dependent  0,1
 *      1 bit: --
 *
 *  8 bytes: To operate from
 *  8 bytes: To operate to/property
 *  Currently not supporting Conditionals, will support in future versions. 
 *  
 *
 */

TransactionType TransactionRunner::statementType(string statement) {
    return (TransactionType) statement[0];
}

pair<lli, lli> getNodes(string statement) {
    if (statement.length() < 17) {
        return make_pair(-1, -1);
        //TODO raise an exception later on
    }
    string val1(9,'\0'), val2(9,'\0');
    for (int i = 0; i < 8; i++) {
        val1[i] = statement[1+i];
        val2[i] = statement[9+i];
    }
    pair<lli,lli> vals;
    try{
        vals= make_pair(stol(val1), stol(val2));
    }catch(std::exception& e){
        cout<<e.what()<<endl;
        cout<<val1<<" "<<val2<<endl;
    }
    return vals;
}

vector<vector<string>> TransactionRunner::runTransaction() {

    vector<vector < string>> results;
    if (tx != NULL)
        for (auto it = tx->operations.begin(); it != tx->operations.end(); it++) {
            try {
                lli id = 0;
                pair<lli, lli> nodes = getNodes((*it));
                vector<lli> neighbours;
                switch (statementType((*it))) {
                    case TransactionType::TxNodeRead:
                        //id = db->getNodeId(nodes.first);
                        //results.push_back(vector<string>(1, to_string(id)));
                        break;
                    case TransactionType::TxEdgeRead:
                        //results.push_back(vector<string>(1, "found"));
                        break;
                    case TransactionType::TxEdgeWrite:
                        id = addEdge(nodes.first, nodes.second);
                        break;
                    case TransactionType::TxNodeWrite:
                        id = addNode(nodes.first);
                        break;
                    case TransactionType::TxNodeDelete:
                        id = deleteNode(nodes.first);
                        break;
                    case TransactionType::TxEdgeDelete:
                        id = deleteEdge(nodes.first, nodes.second);
                    case TransactionType::TxGetNeighbours:
                        neighbours = readNeighbour(nodes.first);
                        results.push_back(convLtoSV(neighbours));
                        break;
                    default:
                        break;
                }
            } catch (std::exception& e) {
                rollBack();
            }

        }
    if (!commit(results)) {
        results.clear();
        results.push_back(vector<string>(1, "Failure"));
    }
    return results;
}

void TransactionRunner::rollBack() {
    opList.clear();
    tx->setSuccess(false);
}

bool TransactionRunner::commit(vector<vector<string>>&results) {
    bool rval = true;
    lli tid = db->validateTransaction(nodeReadSet, nodeWriteSet, nodeDeleteSet, edgeReadSet, edgeWriteSet, this->getTransactionId());
    if (tid > 0) {
        for (auto it = tx->operations.begin(); it != tx->operations.end(); it++) {
            try {
                lli id = 1;
                pair<lli, lli> nodes = getNodes((*it));
                vector<lli> neighbours;
                switch (statementType((*it))) {
                    case TransactionType::TxNodeRead:
                        id = db->getNodeId(nodes.first);
                        results.push_back(vector<string>(1, to_string(id)));
                        break;
                    case TransactionType::TxEdgeRead:
                        results.push_back(vector<string>(1, "found"));
                        break;
                    case TransactionType::TxEdgeWrite:
                        cout<<nodes.first<<" "<<nodes.second<<endl;
                        id = db->addEdge((nodes.first), (nodes.second), tid);
                        break;
                    case TransactionType::TxNodeWrite:
                        id = db->addNode(nodes.first, tid);
                        break;
                    case TransactionType::TxNodeDelete:
                        id = db->deleteNode(nodes.first, tid);
                        break;
                    case TransactionType::TxEdgeDelete:
                        id = db->deleteEdge(nodes.first, nodes.second, tid);
                    case TransactionType::TxGetNeighbours:
                        neighbours = readNeighbour(nodes.first);
                        results.push_back(convLtoSV(neighbours));
                        break;
                    default:
                        break;
                }
                if (id < 0) {
                    cout<<statementType(*it)<<" "<<nodes.first<<" "<<nodes.second<<endl;
                    cout << "Transaction Failed on collision, rolling back" << endl;
                    rollBack();
                    rval = false;
                    break;
                }
            } catch (std::exception& e) {
                cout<<e.what()<<endl;
                cout << "Transaction Failed on exception, rolling back" << endl;
                rollBack();
                rval = false;
            }
        }
    } else {
        cout << "Invalid transaction, rolling back now" << endl;
        rollBack();
        rval = false;
    }
    tx->setCommit(true);
    return rval;
}

lli TransactionRunner::getId(lli node) {
    bool nnode = newNode(node);
    bool dnode = deletedNode(node);
    if (!nnode&&!dnode) {
        return db->getNodeId(node);
    }
    if (nnode) {
        return node*-1;
    }
    if (dnode) {
        return 0;
    }
}
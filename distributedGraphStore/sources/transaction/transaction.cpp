#include"transaction.hh"


void Transaction::addStatement(string statement){
    operations.push_back(statement);
}


bool Transaction::getSuccess(){
    return success;
}

void Transaction::setSuccess(bool value){
    success=value;
}

void Transaction::setCommit(bool value){
    committed=value;
}

bool Transaction::getCommit(){
    return committed;
}

void Transaction::setCompleted(bool v){
    completed=v;
}

bool Transaction::getCompleted(){
    return completed;
}

void Transaction::setTransactionId(long long int id){
    tid=id;
}
long long int  Transaction::getTransactionId(){
    return tid;
}



Transaction::Transaction(){
    success=false;
    committed=false;
    completed=false;
    tid=-1;
}


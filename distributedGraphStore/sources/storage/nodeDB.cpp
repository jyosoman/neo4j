#include"nodeDB.hh"
NodeDbDriver::NodeDbDriver(long long int n):lock(ATOMIC_FLAG_INIT){

    if(n==0){
        n=100000;
    }
    adjacencyList.resize(n);
    capacity=n;
    filled=0; //Node numbering starts from 1. 
}

void NodeDbDriver::deleteNode(long long int node){
    node=getNodeId(node);
    if(node>0)
        adjacencyList[getNodeId(node)]=NULL;
}

shared_ptr<EdgeListHead> NodeDbDriver::getAdjacency(long long int node){
    if(node<capacity)
        return adjacencyList[node];
    return NULL;
}

long long int NodeDbDriver::addNode(long long int node,long long int transactionId){
    if( physicalToLogicalMap.find(node)!= physicalToLogicalMap.end()){
        cout<<"Adding node failed: "<<node<<endl;
        return -1;
    }
    long long int curr;
    curr=__atomic_add_fetch(&filled,1,__ATOMIC_RELAXED);
    try{
        if(curr>=capacity){
            while (lock.test_and_set(std::memory_order_acquire)){}
            if(curr>=capacity){
                adjacencyList.resize(2*capacity,NULL);
                capacity=2*capacity;
            }
            lock.clear(std::memory_order_release);  
        }        
        auto sp1=make_shared<EdgeListHead> (curr, transactionId);
        adjacencyList[curr]=sp1;
        physicalToLogicalMap[node]=curr;
    }  catch (std::bad_alloc& ba){
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    return curr;
}

//Use the map in a later version.
long long int NodeDbDriver::addEdge(long long int from, long long int to, long long int cid){
    from=getNodeId(from); to=getNodeId(to);
    if(from<=filled&&to<=filled&&from>0&&to>0){
        return getAdjacency(from)->addEdge(to,cid);
    }
//    cout<<"from: "<<from<<" to: "<<to<<" tid:"<<cid<<" "<<filled<<endl;
    return -1;
}


long long int NodeDbDriver::getNodeId(long long int node){
    if(physicalToLogicalMap.find(node)!=physicalToLogicalMap.end())
        return physicalToLogicalMap[node];
    cout<<"node: "<<node<<endl;
    return 0;
}

long long int NodeDbDriver::removeNode(long long int node, lli cid){
    if(physicalToLogicalMap.find(node)!=physicalToLogicalMap.end()){
        long long int id=physicalToLogicalMap[node];
        adjacencyList[id]->markDeleted(cid);
        return id;
    }
    cout<<"dnode: "<<node<<endl;
    return 0;
}

long long int NodeDbDriver::deleteEdge(long long int from, long long int to, lli cid){
    long long int fromId=getId(from);
    if(fromId==-1){
        cout<<"dfrom: "<<from<<"to: "<<to<<"tid:"<<cid<<endl;
        return -1;
    }
    long long int toId=getId(to);
    if(toId==-1){
        cout<<"dfrom: "<<from<<"to: "<<to<<"tid:"<<cid<<endl;
        return -1;
    }

  adjacencyList[fromId]->deleteEdge(toId,cid);
  adjacencyList[toId]->deleteEdge(fromId,cid);
}

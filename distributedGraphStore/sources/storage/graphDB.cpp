#include"graphDB.hh"

void
GraphDB::initGDB(string dbFolder, string configFolder,string logFolder){
    configHandler= new ConfigHandler(configFolder);
    //logHandler= new LogHandler(logFolder);
    nodeDB=new NodeDbDriver(atoi(configHandler->getValue("baseNodes").c_str())); 
    graphDeps=new TransactionDependencyManager(150);
    currentId=0;
    snapshotId=0;
}

GraphDB::GraphDB(string dbFolder, string configFolder,string logFolder){
    initGDB(dbFolder,configFolder,logFolder);
}

GraphDB::GraphDB(string rootFolder){
    dbFolder=rootFolder+"/db";
    configFolder=rootFolder+"/config";
    logFolder=rootFolder+"/log";
    initGDB(dbFolder,configFolder,logFolder);
}

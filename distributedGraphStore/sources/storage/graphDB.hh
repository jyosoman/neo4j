#ifndef __GRAPH_DB_HH
#define __GRAPH_DB_HH
#include<map>
#include<list>
#include<omp.h>
#include<queue>
#include<vector>
#include<thread>
#include <atomic>
#include<chrono>
#include<unordered_map>
#include<condition_variable>
#include"storage/nodeDB.hh"
#include"logs/logHandler.hh"
#include"transaction/transaction.hh"
#include"configs/configHandler.hh"
#include"transaction/transactionRunner.hh"
#include"transaction/transactionDependencyManager.hh"

using namespace std;

class GraphDB {
    friend class TransactionRunner;
    mutex m2;

    string dbFolder, configFolder, logFolder;

    std::atomic<long long int> currentId;

    ConfigHandler* configHandler;

    LogHandler* logHandler;

    NodeDbDriver* nodeDB;

    TransactionDependencyManager* graphDeps;

    lli snapshotId;

    unordered_map<lli, lli> edgeWriteSet;

    unordered_map<lli, lli> nodeWriteSet;

    void setDBFolder(string) {
    };

    void getDBFolder(string) {
    };

    void initGDB(string dbFolder, string configFolder, string logFolder);

    //TODO

    void loadDB() {
    }

    //TODO

    void storeDB() {
    };


    long long int addEdge(long long int to, long long int from, long long int id) {
        return nodeDB->addEdge(to, from, id);
    }

    long long int addNode(long long int id, long long int tid) {
        return nodeDB->addNode(id, tid);
    }

    long long int deleteNode(long long int id, long long int tid) {
        return nodeDB->removeNode(id, tid);
    }    
  
    long long int deleteEdge(long long int id, long long int to, long long int tid) {
        return nodeDB->deleteEdge(id, to, tid);
    }

    vector<long long int> getNeighbours(long long int id, long long int tid) {
        return nodeDB->getNeighbours(id, tid);
    }    
    
    bool validateEdgeRead(lli node, lli snapshotId) {
        auto iter = edgeWriteSet.find(node);
        if (iter != edgeWriteSet.end()) {
            if (edgeWriteSet[node] > snapshotId) {
                //cout<<edgeWriteSet[node]<<" "<<snapshotId<<endl;
                return false;
            }
        }
        return true;
    }

    bool validateNodeRead(lli node, lli snapshotId) {
        auto iter = nodeWriteSet.find(node);
        if (iter != nodeWriteSet.end()) {
            if (nodeWriteSet[node] > snapshotId) {
                return false;
            }
        }
        return true;
    }

    bool addEdgeWrite(lli node, lli transactionId) {
        edgeWriteSet[node] = transactionId;
    }

    bool addNodeWrite(lli node, lli transactionId) {
        nodeWriteSet[node] = transactionId;
    }
    

    
public:

    long long int nextSnapshotId() {
        return snapshotId;
    }

    long long int getId() {
        return currentId++;
    }

    GraphDB(string dbFolder, string configFolder, string logFolder);


    GraphDB(string rootFolder);

    int getThreadCount() {
        return atoi(configHandler->getValue("NumThreads").c_str());
    }

    bool addTransaction(long long int trid) {
        graphDeps->addTransaction(trid);
    }

    bool finishTransaction(long long int trid) {
        graphDeps->removeTransaction(trid);
    }

    long long int getNodeId(long long int extId) {
        return nodeDB->getNodeId(extId);
    }


    bool registerTransaction(long long int transactionId) {
        return addTransaction(transactionId);
    }



    /*
     *
     *  Rewrite this transaction soonish. 
     *
     */

    lli validateTransaction(unordered_set<lli> nrs, unordered_set<lli> nws, unordered_set<lli> nds,
            unordered_set<lli> ers, unordered_map<lli, vector<lli>> ews, lli snapshotId) {
        unique_lock<mutex> locker(m2);
        //locker.lock();
        lli transactionId = getId();
        for (lli x : ers) {
            if (x > 0) {
                if (!validateEdgeRead(x, snapshotId)) {
                    return -1;
                }
            }
        }
        for (lli x : nrs) {
            if (x > 0) {
                if (!validateNodeRead(x, snapshotId)) {
                    return -1;
                }
            }
        }
        for (lli x : nws) {
            if (x > 0) {
                addNodeWrite(x, transactionId);
            }
        }
        for (auto x : ews) {
            if (x.first > 0) {
                addEdgeWrite(x.first, transactionId);
            }
        }
        for (lli x : nds) {
            if (x > 0) {
                addNodeWrite(x, transactionId);
            }
        }
        locker.unlock();
        snapshotId = transactionId;
        return transactionId;
    }
};

class GDBTransactionInterface {
    GraphDB * db;
    std::condition_variable cv;
    mutex m, m2;
    int numThreads;

    TransactionDependencyManager *tdManager;

    unordered_map<long long int, vector<vector<string>>> t_resMap;
    vector<thread> threads;
    bool running;
    queue<Transaction*> transactionQueue;
    long long int cid;

    void threadFunction() {
        while (running) {
            Transaction* currTransaction = getTransaction();
            if (currTransaction == NULL) {
                unique_lock<mutex> lk(m);
                cv.wait_for(lk,10ms, [this] {return (transactionQueue.size() > 0&&running);});
                lk.unlock();
            } else {
                std::thread::id this_id = std::this_thread::get_id();
                if (t_resMap.find(currTransaction->getBeginId()) == t_resMap.end()) {
                    t_resMap[currTransaction->getBeginId()] = runTransaction(currTransaction);
                }
            }
        }
    }

    void signalFinish() {
        running = false;
        cv.notify_all();
    }
    
    vector<vector<string>> runTransaction(Transaction* t) {
        t->setTransactionId(db->getId());
        TransactionRunner *trunner = new TransactionRunner(t, db);
        trunner->setTransactionId(db->getId());
        vector<vector < string>> result = trunner->runTransaction();
        delete trunner;
        return result;
    }
    
    Transaction* getTransaction() {
        Transaction* ret = NULL;
        unique_lock<mutex> locker(m);
        if (transactionQueue.size() > 0) {
            ret = transactionQueue.front();
            transactionQueue.pop();
        }

        //locker.unlock();
        return ret;
    }
public:

    GDBTransactionInterface(GraphDB*db) {
        this->db = db;
        numThreads = db->getThreadCount();
        cid=0;
        if (numThreads <= 1) {
            numThreads = 2;
        }
        tdManager = new TransactionDependencyManager(numThreads);
        running = true;
        for (int i = 0; i < numThreads; i++) {
            threads.push_back(thread(&GDBTransactionInterface::threadFunction, this));
        }
    }

    ~GDBTransactionInterface() {
        signalFinish();
        for (thread& t : threads){
            t.join();
        }     
    }



    vector<vector<string>> getTransactionResults(Transaction* t) {
        if (t_resMap.find(t->getBeginId()) == t_resMap.end()) {
            t_resMap[t->getBeginId()] = runTransaction(t);
        }
        return t_resMap[t->getBeginId()];
    }


    void addTransaction(Transaction *t) {
        unique_lock<mutex> locker(m);
        t->setBeginId(cid);
        cid++;
        transactionQueue.push(t);
        locker.unlock();
        cv.notify_one();
    }

};
#endif

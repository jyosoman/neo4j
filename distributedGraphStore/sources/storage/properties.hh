#ifndef __PROPERTIES__HH
#define __PROPERTIES__HH
#include<vector>
#include<string>
class Properties{
    std::vector<std::string> properties;
    public:
    Properties(){
    }

    /* Properties(const Properties nps){ */
    /*     properties.resize(nps.properties.size()); */
    /*     for(int i=0;i<nps.properties.size();i++){ */
    /*         properties.push_back(nps.properties[i]); */
    /*     } */
    /* } */
    Properties(std::vector<std::string> props){
        properties.resize(props.size());
        for(unsigned int i=0;i<props.size();i++){
            properties.push_back(props[i]);
        }
    }
    std::vector<std::string> getProperties();
    void setProperty(std::string prop);
};


#endif

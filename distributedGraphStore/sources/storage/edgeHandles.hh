#ifndef __EDGEHANDLES__HH
#define __EDGEHANDLES__HH
#include"properties.hh"
#include<iostream>
#include<atomic>
#include<cstdio>
#include"fileStore/fileHandler.hh"

using namespace std;
typedef long long int lli;
class BaseEdgeList{
    public:
        virtual BaseEdgeList* getNext()=0;
        virtual void setNext(BaseEdgeList*next)=0;
        virtual BaseEdgeList* getPrev()=0;
        virtual void setPrev(BaseEdgeList*prev)=0;

};
class EdgeList
{
    public:
    
    long long int from, to;
    Properties *edgeProperties;
    /*this is useful for handling deletes, will remove it for a better runtime based deletion system later. 
     * This is effectively snapshot isolation. 
     */
    
    long long int creatingTransaction, deletingTransaction, readingTransaction;
    
    EdgeList* next,*prev;
    
    EdgeList(long long int,long long int,long long int cid);
    
    virtual ~EdgeList(){
        if (edgeProperties!=NULL){
            delete edgeProperties;
        }
        
    }
    
    virtual EdgeList* getNext(){
        return next;
    }
    EdgeList* getNext(EdgeList* p){
        if(p!=NULL)
            return p->next;
        return NULL;
    }
    virtual void setNext(EdgeList* next){
        this->next=next;
    }
    virtual EdgeList* getPrev(){
        return prev;
    }
    virtual void setPrev(EdgeList*prev){
        this->prev=prev;
    }

    void updatePrev(EdgeList* prev){
        this->prev=prev;
    }
    void markDeleted(long long int did){
        deletingTransaction=did;
    }
    void printNode(){
        printf("%p %lld %lld %p %p %lld %lld\n",this,from,to,next,prev, creatingTransaction, deletingTransaction);
    }
    bool checkTid(long long int tid){
        if(creatingTransaction<=tid){
            if(deletingTransaction!=-1){
                if(deletingTransaction>tid)
                    return true;
                return false;
            }
            return true;            
        }
        return false;
    }
};

class EdgeListHead{
    long long int id;
    EdgeList* head, *tail;
    long long int creatingTransaction, deletingTransaction, readingTransaction;
    void addEdge(EdgeList* edge){
        addEdges(edge,edge);
    }
    
    void addEdges(EdgeList* front, EdgeList*back){
        EdgeList* ret;
        __atomic_exchange(&tail,&back,&ret,__ATOMIC_ACQUIRE);
        if(ret!=NULL){
            ret->next=front;
        }else{
            head=front;
        }
    }

   public:
    
    class EdgeListIterator{
        public:
        typedef EdgeListIterator self_type;
        typedef EdgeList value_type;
        typedef EdgeList& reference;
        typedef EdgeList* pointer;
        typedef std::forward_iterator_tag iterator_category;
        EdgeListIterator(pointer ptr) : ptr_(ptr) { }
        EdgeListIterator(){ }
        self_type operator=(const self_type& other) { ptr_ = other.ptr_; return *this; }
        self_type operator++(int junk) { self_type i = *this; ptr_=ptr_->next; return i; }
        self_type operator++() { ptr_=ptr_->next; return *this; }
        reference operator*() { return *ptr_; }
        pointer operator->() { return ptr_; }
        bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
        bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
        pointer getPointer(){return ptr_;}
        private:
        pointer ptr_;

    };

    class ConstEdgeListIterator{
        public:
        typedef ConstEdgeListIterator self_type;
        typedef EdgeList value_type;
        typedef EdgeList& reference;
        typedef EdgeList* pointer;
        typedef std::forward_iterator_tag iterator_category;
        ConstEdgeListIterator(pointer ptr) : ptr_(ptr) { }
        ConstEdgeListIterator(){ }
        self_type operator=(const self_type& other) { ptr_ = other.ptr_; return *this; }
        self_type operator++(int junk) { self_type i = *this; ptr_=ptr_->next; return i; }
        self_type operator++() { ptr_=ptr_->next; return *this; }
        const value_type& operator*() { return *ptr_; }
        const value_type* operator->() { return ptr_; }
        bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
        bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
        pointer getPointer(){return ptr_;}
        private:
        pointer ptr_;

    };

    EdgeListHead(long long int id, long long int cid){
        this->id=id;
        head=NULL;//new EdgeList(id,-1,cid);
        tail=head;
    }
    ~EdgeListHead(){
        while(head!=tail){
            EdgeList*temp=head;
            delete temp;
            head=head->next;            
        }
        delete head;
    }
    
    long long int addEdge(long long int to, long long int creatingId){
        EdgeList* edge=new EdgeList(to, id, creatingId);
        addEdge(edge);
        return (long long int)edge;
    }
    long long int deleteEdge(long long int to, lli cid){
        for(auto iter=this->begin(); iter!=this->end();iter++){
            if((*iter)->to==to){
                (*iter)->markDeleted(cid);
                return (long long int) (iter->getPointer());
            }
        }
        return 0;
    }

    vector<long long int> getNeighbours(long long int rid){
        vector<long long int> results;
        for(auto iter=head; iter!=NULL;iter=iter->next){
            if((iter)->checkTid(rid))
                results.push_back((iter)->to);
        }
        return results;
    }
    
    void printList(){
        EdgeList* node=head;
        while(node!=NULL){
            node->printNode();
            node=node->next;
        }
        cout<<endl;
    }
    
    void markDeleted(long long int did){
        deletingTransaction=did;
    }

    EdgeListIterator* begin(){
        return new EdgeListIterator(head);
    }
    
    EdgeListIterator* end(){
        return new EdgeListIterator(tail);
    }
};


/* class FileEdgeStore:public FileMemoryManager<EdgeList>{ */
/*     protected: */
/*     public: */
/*         FileMemoryManager<EdgeList> fmEdge; */
/*         FileMemoryManager<Properties> fmProps; */
/*         EdgeList* newEdge(){ */
/*             return (EdgeList*) fmEdge.allocate(sizeof(EdgeList),NULL); */
/*         } */
/*         Properties* newProperty(){ */
/*             return (Properties*) fmProps.allocate(sizeof(Properties),NULL); */
/*         } */
/* }; */



class FileEdgeData{
    protected:
    long long int from, to;
    long long int properties;
    long long int creatingTransaction,deletingTransaction, readingTransaction;
    long long int nextEdge, prevEdge;

    public:
    FileEdgeData(long long int from, long long int to, long long int cid){
        this->from=from;
        this->to=to;
        this->creatingTransaction=cid;
        deletingTransaction=-1;
        readingTransaction=-1;
        nextEdge=-1;
        prevEdge=-1;
    }
    long long int getNext(){
        return  nextEdge;
    }
    void setPrev(long long int prev){
        this->prevEdge=prev;
    }
    void setNext(long long int next){
        nextEdge=next;
    }
    void deleteEdge(long long int deletingId){
        deletingTransaction=deletingId;
    }
};
class FileEdgeList:public BaseEdgeList, public FileEdgeData{
    public:
        static long long int ELOffset, PropOffset; 
        FileEdgeList(FileEdgeData*);
        virtual FileEdgeList*  getNext(){
            if(nextEdge==-1)
                return NULL;
            else
                return (FileEdgeList*) nextEdge+ELOffset;
        }
        virtual void setNext(FileEdgeList* next){
            nextEdge=(long long int)next-ELOffset;
        }
        virtual FileEdgeList*  getPrev(){
            if(prevEdge==-1)
                return NULL;
            else
                return (FileEdgeList*) prevEdge+ELOffset;
        }
        virtual void setPrev(FileEdgeList* prev){
            prevEdge=(long long int)prev-ELOffset;
        }
        Properties* getProperties(){
            if(properties==-1)
                return NULL;
            return (Properties*) properties+PropOffset;
        }       
};




#endif 

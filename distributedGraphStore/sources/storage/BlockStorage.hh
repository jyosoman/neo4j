/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BlockStorage.hh
 * Author: jyothish
 *
 * Created on 02 July 2018, 16:58
 */

#ifndef BLOCKSTORAGE_HH
#define BLOCKSTORAGE_HH

#include <utility> 
#include<unordered_map>
#include<vector>
#include<iostream>
#include<cstring>
#include"common/commonDefs.hh"

class BlockStorage {
protected:
    int nodes, edgeCount, snapshotId;

    typedef struct {
        int id;
        int nodeEdgeCount;
        int offsets;
        std::pair<int, int> nextBlock;
        std::pair<int, int> previousBlock;
    } dataIndex;

    dataIndex* blockIndex;

    Edges* edges;

    typedef struct {
        std::vector<int> edges;
        dataIndex dindex;
    } nodeData_t;

    int findNodeIndex(int node) {
        for (int i = 0; i < nodes; i++) {
            if (blockIndex[i].id == node)
                return i;
        }
        return -1;
    }

public:
    bool deleteEdge(int from, int index, int to) {
        if (index != -1 && blockIndex[index].id == from) {
            for (int j = blockIndex[index].offsets; j < blockIndex[index].offsets + blockIndex[index].nodeEdgeCount; j++) {
                if (edges[j].to == to) {
                    memcpy(&edges[j], &edges[blockIndex[index].offsets + blockIndex[index].nodeEdgeCount - 1], sizeof (Edges));
                    blockIndex[index].nodeEdgeCount--;
                    return true;
                    break;
                }
            }
        }
        return false;
    }


    ~BlockStorage() {
        //std::cout<<"Deleting Block"<<std::endl;
        delete edges;
        delete blockIndex;
    }

    BlockStorage(int nodes, int edgecount, std::unordered_map<int, std::vector<int>> edgeValues, int tid) {
        snapshotId = tid;
        this->nodes = nodes;
        blockIndex = new dataIndex[nodes];
        edgeCount = edgecount;
        edges = new Edges[edgecount];
        int currentEdge = 0, currentNode = 0;
        for (auto val : edgeValues) {
            blockIndex[currentNode].id = val.first;
            blockIndex[currentNode].nodeEdgeCount = val.second.size();
            blockIndex[currentNode].offsets = currentEdge;
            blockIndex[currentNode].nextBlock.first = -1;
            blockIndex[currentNode].previousBlock.first = -1;
            currentNode++;
            if (currentNode > nodes) {
                std::cout << "Something went wrong" << std::endl;
            }
            for (auto edge : val.second) {
                edges[currentEdge].to = edge;
                currentEdge++;
                if (currentEdge > edgeCount) {
                    std::cout << "Something is not right" << std::endl;
                }
            }
        }
    }

    BlockStorage(int nodes, int edgecount, std::unordered_map<int, nodeData_t> edgeValues, int tid) {
        snapshotId = tid;
        this->nodes = nodes;
        blockIndex = new dataIndex[nodes];
        edgeCount = edgecount;
        edges = new Edges[edgecount];
        int currentEdge = 0, currentNode = 0;
        for (auto val : edgeValues) {
            blockIndex[currentNode].id = val.first;
            blockIndex[currentNode].nodeEdgeCount = val.second.edges.size();
            //std::cout<< blockIndex[currentNode].id << " "<<blockIndex[currentNode].edges<<std::endl;
            blockIndex[currentNode].offsets = currentEdge;
            blockIndex[currentNode].nextBlock = val.second.dindex.nextBlock;
            blockIndex[currentNode].previousBlock = val.second.dindex.previousBlock;
            currentNode++;
            if (currentNode > nodes) {
                std::cout << "Something went wrong" << std::endl;
            }
            for (auto edge : val.second.edges) {
                edges[currentEdge].to = edge;
                currentEdge++;
                if (currentEdge > edgeCount) {
                    std::cout << "Something is not right" << std::endl;
                }
            }
        }
    }

    std::vector<int> getEdges(int node, int index) {
        std::vector<int> result;
        if (index != -1 && blockIndex[index].id == node) {
            for (int j = blockIndex[index].offsets; j < blockIndex[index].offsets + blockIndex[index].nodeEdgeCount; j++) {
                result.push_back(edges[j].to);
            }
        } else {
            std::cout << "Invalid details found" << node << " " << index << " " << blockIndex[index].id << std::endl;
        }
        return result;
    }

    std::vector<int> getEdges(int node) {
        return getEdges(node, findNodeIndex(node));
    }

    bool deleteEdge(int from, int to) {
        return deleteEdge(from, findNodeIndex(from), to) && deleteEdge(to, findNodeIndex(to), from);
    }

    bool deleteNode(int node, int index) {
        if (blockIndex[index].id != node)
            return false;
        for (int j = blockIndex[index].offsets; j < blockIndex[index].offsets + blockIndex[index].nodeEdgeCount; j++) {
            deleteEdge(edges[j].to, findNodeIndex(edges[j].to), node);
        }
        blockIndex[index].id = -1;
        blockIndex[index].nodeEdgeCount = 0;
        return true;
    }

    bool deleteNode(int node) {
        return deleteNode(node, findNodeIndex(node));

    }

    bool nodeAvailable(int node) {
        return findNodeIndex(node) != -1;
    }

    std::vector<int> getNodesInBlock() {
        std::vector<int> result;
        for (int i = 0; i < nodes; i++) {
            result.push_back(blockIndex[i].id);
        }
        return result;
    }

    std::vector<std::pair<int, int>> getNodesInBlockWithIndex() {
        std::vector<std::pair<int, int>> result;
        for (int i = 0; i < nodes; i++) {
            result.push_back(std::make_pair(blockIndex[i].id, i));
        }
        return result;
    }

    std::vector<std::pair<int, int>> getFrontNodesInBlockWithIndex() {
        std::vector<std::pair<int, int>> result;
        for (int i = 0; i < nodes; i++) {
            if (blockIndex[i].previousBlock.first == -1)
                result.push_back(std::make_pair(blockIndex[i].id, i));
        }
        return result;
    }

    void addNextIndex(int node, int currIndex, int nextBlock, int nextIndex) {
        if (blockIndex[currIndex].id != node)
            return;
        blockIndex[currIndex].nextBlock.first = nextBlock;
        blockIndex[currIndex].nextBlock.second = nextIndex;
    }

    void addPrevIndex(int node, int currIndex, int nextBlock, int nextIndex) {
        if (blockIndex[currIndex].id != node)
            return;
        blockIndex[currIndex].previousBlock.first = nextBlock;
        blockIndex[currIndex].previousBlock.second = nextIndex;
    }

    std::pair<int, int> getNextIndexes(int node, int index) {
        if (blockIndex[index].id != node) {
            return std::make_pair(-1, -1);
        }
        return blockIndex[index].nextBlock;
    }

    BlockStorage* mergeBlocks(BlockStorage*block) {
        /*
         * Current block is previous and the incoming block is the next one. 
         * This has to be guaranteed from the code. Also, currently no code to disallow fat nodes. 
         * Updates are going to be affected by this.   
         * 
         * The file based merge or a better merge of this system would be different from this one.        
         */

        std::unordered_map<int, int> nodesLocal;
        std::unordered_map<int, nodeData_t> edges;

        for (int i = 0; i < nodes; i++) {
            nodesLocal[blockIndex[i].id] = i;
        }

        std::vector<std::pair<int, int>> blockNodes = block->getNodesInBlockWithIndex(); //Node and index

        int nodeCount = 0, edgeCount = 0;

        for (auto blockData : blockNodes) {
            edges[blockData.first].edges = block->getEdges(blockData.first, blockData.second);
            edges[blockData.first].dindex.nextBlock = block->blockIndex[blockData.second].nextBlock;
            edges[blockData.first].dindex.previousBlock = block->blockIndex[blockData.second].nextBlock;
            if (nodesLocal.find(blockData.first) != nodesLocal.end()) {
                auto localNodeEdges = getEdges(blockData.first, nodesLocal[blockData.first]);
                edges[blockData.first].dindex.previousBlock = blockIndex[nodesLocal[blockData.first]].previousBlock;
                //std::cout<<blockData.first<<" "<<edges[blockData.first].edges.size()<<" "<<localNodeEdges.size()<<" "<< nodesLocal[blockData.first]<<" ";
                edges[blockData.first].edges.insert(edges[blockData.first].edges.end(), localNodeEdges.begin(), localNodeEdges.end());
                //std::cout<<edges[blockData.first].edges.size()<<std::endl;
                nodesLocal.erase(blockData.first);
            }
            edges[blockData.first].dindex.nodeEdgeCount = edges[blockData.first].edges.size();
            edges[blockData.first].dindex.id = blockData.first;
            nodeCount++;
            edgeCount += edges[blockData.first].edges.size();
        }

        for (auto blockData : nodesLocal) {
            edges[blockData.first].edges = getEdges(blockData.first, blockData.second);
            edges[blockData.first].dindex.nextBlock = blockIndex[blockData.second].nextBlock;
            edges[blockData.first].dindex.previousBlock = blockIndex[blockData.second].previousBlock;
            edges[blockData.first].dindex.nodeEdgeCount = edges[blockData.first].edges.size();
            edges[blockData.first].dindex.id = blockData.first;
            nodeCount++;
            edgeCount += edges[blockData.first].edges.size();
        }
        BlockStorage* bs = new BlockStorage(nodeCount, edgeCount, edges, 0);
        for (int node = 0; node < nodeCount; node++) {
            bs->blockIndex[node].nextBlock = edges[bs->blockIndex[node].id].dindex.nextBlock;
            bs->blockIndex[node].previousBlock = edges[bs->blockIndex[node].id].dindex.previousBlock;
        }
        return bs;

    }
};

#endif /* BLOCKSTORAGE_HH */


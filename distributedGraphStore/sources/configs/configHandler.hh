#ifndef __CONFIGHANDLER__HH__
#define __CONFIGHANDLER__HH__
#include<map>
#include<fstream>

using namespace std;

class ConfigHandler{
    string configFile;
    map<string,string> configList;
    void loadConfigFile();
    public:
    void loadConfigFile(string filename);
    ConfigHandler();
    ConfigHandler(string filepath);
    string getValue(string key);
};
#endif


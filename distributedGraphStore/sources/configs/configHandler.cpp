#include"configHandler.hh"
#include<sstream>


ConfigHandler::ConfigHandler(){
    configFile="db/jdbConfig.conf";
    loadConfigFile();
}

ConfigHandler::ConfigHandler(string filePath){
    configFile=filePath+"/jdbConfig.conf";
    loadConfigFile();
}

void ConfigHandler::loadConfigFile(){
    ifstream filehandler(configFile.c_str());
    if(filehandler.is_open()){
        string line;
        while(getline(filehandler,line)){
            std::istringstream is_line(line);
            std::string key;
            if (std::getline(is_line, key, '='))
            {
                string value;
                if (std::getline(is_line, value))
                {
                    configList[key] = value;
                }
            }
        }
    }
}

void ConfigHandler::loadConfigFile(string filename){
    configFile=filename;
    loadConfigFile();
}

/*
 *
 *Exception testing needed for this function
 *
*/



string ConfigHandler::getValue(string key){
    auto iter=configList.find(key);
    if (iter!=configList.end())
        return configList[key];
    else
        return "";
}


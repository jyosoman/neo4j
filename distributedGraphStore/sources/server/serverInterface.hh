#include"server.hh"

class ServerInterface{
    Server* server;
    queue<Transaction*> tQueue;
    queue<long long int> idQueue;
    public:
    /*  
     *  This should never be called ideally. This should be in the python code and not here. 
     *
     */ 
    ServerInterface(){
        server=new Server();
    }
    ServerInterface(vector<string> vs){
        server= new Server(vs);
    }
    void setServer(Server*s){
        server=s;
    }
    void acceptTransaction(Transaction* t,long long int id);
    
    bool checkTransaction(long long int id); 
    
    vector<vector<string>> sendTransactionResults(long long int id);
    
};


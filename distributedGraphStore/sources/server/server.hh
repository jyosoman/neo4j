#include<iostream>
#include<atomic>
#include<cstdio>
#include <sys/mman.h>
#include"storage/nodeDB.hh"
#include<list>
#include<map>
#include<thread>
#include"storage/graphDB.hh"
#include"configs/configHandler.hh"

using namespace std;

class Server {
protected:
    GraphDB* graphdb;
    GDBTransactionInterface* GTI;
    string rootPath;
    void initialiseDB();
    long long int completedId, snapshotId;
    vector<long long int> activeIds;
    bool active;

    long long int getId() {
        return snapshotId;
        //.fetch_add(1, std::memory_order_relaxed);
    }
public:
    Server();

    Server(vector<string>);
    
    ~Server(){
        stopServer();
        delete GTI;
        delete graphdb;
    }

    void updateSnapshotId(long long int id) {
        snapshotId = graphdb->nextSnapshotId();
    }

    bool transactionCompleted(Transaction*t) {
        return t->getCommit();
    }

    vector<vector<string>> returnTransactionResults(Transaction*);

    void receiveTransaction(Transaction*);

    bool returnStatus() {
        return true;
    }

    void stopServer() {
        active=false;
        //GTI->signalFinish();
    }
};



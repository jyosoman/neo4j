/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.storage.dataqueue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Test to check if the queue can persist data and the data can be retrieved in a FIFO manner.
 * This is a super test for the
 */

public class PersistentQueueTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();


    /**
     * Check if the value persisted to disk is the same as the data placed.
     *
     * @throws IOException
     */
    @Test
    public void testAll() throws IOException {
        PersistentQueue persistentQueue;
        persistentQueue = new PersistentQueue(folder.newFile("persistFile"));
        byte[] bytes = new byte[16];
        bytes[3] = 0xf;
        persistentQueue.addData(bytes);
        assertEquals(persistentQueue.getElementCount(), 1);
        assertArrayEquals(bytes, persistentQueue.peek());
        assertArrayEquals(bytes, persistentQueue.getData());
        folder.delete();
    }

}
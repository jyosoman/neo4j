/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.transaction;

import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.storage.UnifiedDataStorage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TransactionalStorageTest {
//TODO: Need to complete this test.

    private static TransactionalStorage transactionalStorage;

    @BeforeClass
    public static void setUp() {
        transactionalStorage = new TransactionalStorage(new Transaction(new TransactionScheduler()));
        transactionalStorage.setStorage(new UnifiedDataStorage());
    }

    @AfterClass
    public static void tearDown() {

    }

    @Test
    public void addEdge() {
        transactionalStorage.addEdge(new Edge(0, 1, 0, '2', null));
        transactionalStorage.commit();

    }

    @Test
    public void addNode() {
    }

    @Test
    public void getEdges() {
    }

    @Test
    public void deleteNode() {
    }

    @Test
    public void findNode() {
    }

    @Test
    public void deleteEdge() {
    }

    @Test
    public void getTransactionId() {
    }

    @Test
    public void getResults() {
    }

    @Test
    public void setTransaction() {
    }

    @Test
    public void commit() {
    }
}
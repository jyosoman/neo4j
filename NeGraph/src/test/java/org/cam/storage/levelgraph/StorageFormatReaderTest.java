/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.datatypes.Direction;
import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.storage.FileInterface;
import org.cam.storage.levelgraph.storage.RocksDBInterface;
import org.cam.storage.levelgraph.storage.StorageFormatReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/*

This test will fail if the hardcoded on-disk size of primitives is not the same as the actual.

*/

public class StorageFormatReaderTest {
    private static StorageFormatReader storageFormatReader;
    private static String fileName;


    @BeforeClass
    public static void writeFile(){
        FileInterface fileInterface=new FileInterface();
        RocksDBInterface test = mock(RocksDBInterface.class);
        when(test.getValue((long) 0, 4)).thenReturn((long) 0);
        fileInterface.setRocksDBInterface(test);
        UpdatesCache updatesCache=new UpdatesCache(fileInterface);
        updatesCache.addNode(new LevelNode(0));
        updatesCache.addNode(new LevelNode(1));
        updatesCache.addEdge((long) 0,new Edge((long)1,-1,new Direction(Direction.DirectionValues.Bidirectional), null) );
        updatesCache.addEdge((long) 1,new Edge((long)0,-1,new Direction(Direction.DirectionValues.Bidirectional), null) );
        fileName=updatesCache.flushToDisk();
        storageFormatReader=new StorageFormatReader(fileName);
        System.out.println("NodeCount:"+storageFormatReader.getNodeCount());
    }

    @AfterClass
    public static void deleteFile(){
        System.out.println(fileName);
        File file=new File(fileName);
        if (file.delete()) {
            System.out.println("File deleted successfully");
        } else {
            System.err.println("Failed to delete the file");
        }
    }

    @Test
    public void getNeighbour() throws Exception {
        assertEquals(storageFormatReader.getNeighbour(0,0),1);
        assertEquals(storageFormatReader.getNeighbour(1,0),0);
    }

    @Test
    public void getEdgeDirection() throws Exception {
        assertEquals(storageFormatReader.getEdgeDirection(0,0).getDirectionValue(),Direction.DirectionValues.Bidirectional);
        assertEquals(storageFormatReader.getEdgeDirection(1,0).getDirectionValue(),Direction.DirectionValues.Bidirectional);
    }

    @Test
    public void getEdgeId() throws Exception {
        assertEquals(storageFormatReader.getEdgeId(0,0),0);
        assertEquals(storageFormatReader.getEdgeId(1,0),1);
    }

    @After
    public void testDeleteEdge() throws Exception {
        storageFormatReader.deleteEdge(new Edge((long) 0, (long) 1, (long) -1, new Direction(Direction.DirectionValues.Bidirectional), null));
        ArrayList<Edge> array = storageFormatReader.getEdges(0);
        System.out.println(array.size());
    }

}


/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.storage;

import org.cam.storage.levelgraph.Pair;
import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.datatypes.Properties;

import java.util.ArrayList;
import java.util.HashMap;


public class FileInterface {

    private RocksDBInterface rocksDBInterface;

    // 0: LevelNode external to internal
    // 1: LevelNode, Edge type to Sub-levelNode. Not really using this right now. Rest of all are persisted.
    // 2: LevelNode to start buffer
    // 3: LevelNode to last buffer
    // 4: Last used bufferCount

    private HashMap<LevelNode, StorageFormatReader> nodeToBufferMap;

    private HashMap<LevelNode, StorageFormatReader> nodeToLastBufferMap;

    private HashMap<Long, StorageFormatReader> IdMapper;

    private ArrayList<StorageFormatReader> readers;

    private Integer bufferCount; //This is a relative number. Need to handle this properly, else will be super buggy.

    public FileInterface() {
        bufferCount = new Integer(0);
        nodeToBufferMap = new HashMap<LevelNode, StorageFormatReader>();
        nodeToLastBufferMap = new HashMap<LevelNode, StorageFormatReader>();
        IdMapper = new HashMap<Long, StorageFormatReader>();
    }

    private String getFileName(int number) {
        return "level" + number + ".dat";
    }

    private String getNextFileName(int level) {
        bufferCount++;
        return getFileName(bufferCount);
    }

    public void setRocksDBInterface(RocksDBInterface rocksDBInterface) {
        this.rocksDBInterface = rocksDBInterface;
        bufferCount = rocksDBInterface.getValue((long) 0, 4).intValue();
        if (bufferCount == -1) {
            bufferCount = 0;
        }
        for (int i = 0; i < bufferCount; i++) {
            IdMapper.put((long) i, new StorageFormatReader(getFileName(i)));
            IdMapper.get((long) i).setBlockId(i);
        }
    }

    public String writeToFile(HashMap<LevelNode, ArrayList<Edge>> edgeList, HashMap<LevelNode, Pair<Integer, Integer>> prevData) {
        String fileName = getNextFileName(0);
        FileWriter fileWriter = new FileWriter(fileName, edgeList, prevData);
        StorageFormatReader storageFormatReader;
        rocksDBInterface.setValue((long) 0, (long) bufferCount, 4);// 4: Last used buffer
        storageFormatReader = new StorageFormatReader(fileName);
        storageFormatReader.setBlockId(bufferCount);
        IdMapper.put((long) bufferCount, storageFormatReader);
        for (LevelNode levelNode : edgeList.keySet()) {
            if (!nodeToBufferMap.containsKey(levelNode)) {
                nodeToBufferMap.put(levelNode, storageFormatReader);
                if (rocksDBInterface.getValue(levelNode.getInternalId(), 2) == -1) {
                    rocksDBInterface.setValue(levelNode.getInternalId(), (long) bufferCount, 2); //2: LevelNode to start buffer
                    rocksDBInterface.setValue(levelNode.getExternalId(), levelNode.getInternalId(), 0);
                }
            }
            if (nodeToLastBufferMap.get(levelNode) != null) {
                nodeToLastBufferMap.get(levelNode).setNextBlock(levelNode.getInternalId(), storageFormatReader.getBlockId());
                storageFormatReader.setPreviousBlock(levelNode.getInternalId(), nodeToLastBufferMap.get(levelNode).getBlockId());
            }
            nodeToLastBufferMap.put(levelNode, storageFormatReader);
            rocksDBInterface.setValue(levelNode.getInternalId(), (long) bufferCount, 3); //3: LevelNode to last buffer
        }
        return fileName;
    }

    LevelNode getNode(LevelNode levelNode) {
        Long internalId = rocksDBInterface.getValue(levelNode.getExternalId(), 0);
        LevelNode result = null;
        if (internalId != -1) {
            result = new LevelNode(internalId);
        }
        return result;
    }

    ArrayList<Edge> getEdges(LevelNode levelNode, long queryingTransaction) {
        ArrayList<Edge> results = new ArrayList<Edge>();
        StorageFormatReader reader = nodeToBufferMap.get(levelNode);
        while (reader != nodeToLastBufferMap.get(levelNode)) {
            results.addAll(reader.getEdges(levelNode.getInternalId()));
            reader = IdMapper.get((long) reader.getNextBlock(levelNode.getInternalId()));
        }
        return results;
    }

    ArrayList<Properties> getProperties(LevelNode levelNode) {
        //TODO
        return null;
    }

    ArrayList<Properties> getProperties(Edge edge) {
        //TODO
        return null;
    }

    void deleteEdge(Edge edge) {
        for (StorageFormatReader storagereader : IdMapper.values()
        ) {
            int edgeindex = storagereader.findEdge(edge);
            if (edgeindex != -1) {
                storagereader.deleteEdge(edge);
            }
        }
    }

    /*

     */
    void mergeFiles() {

    }
    /* Need to implement file merging stuff. */


}

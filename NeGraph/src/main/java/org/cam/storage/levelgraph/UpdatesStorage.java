/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.datatypes.Properties;
import org.cam.storage.levelgraph.storage.DataStorageInterface;

import java.util.ArrayList;
import java.util.HashMap;

//TODO remove externalID from the code entirely. Adding ambiguity.

public class UpdatesStorage implements DataStorageInterface {

    HashMap<LevelNode, ArrayList<Edge>> cachedEdges;

    HashMap<Long, LevelNode> nodeMap;

    HashMap<LevelNode, Pair<Integer, Integer>> prevData;

    HashMap<LevelNode, ArrayList<Properties>> nodePropertyStore;

    HashMap<Edge, ArrayList<Properties>> edgePropertyStore;

    Integer currentBlock;

    Long currentEdgeId;

    Long currentNodeId;

    public UpdatesStorage() {
        cachedEdges = new HashMap<LevelNode, ArrayList<Edge>>();
        nodeMap = new HashMap<Long, LevelNode>();
        prevData = new HashMap<LevelNode, Pair<Integer, Integer>>();
        currentBlock = 0;
        currentEdgeId = (long) 0;
        currentNodeId = (long) 0;
    }

    public LevelNode getNode(Long node) {
        if (nodeMap.containsKey(node)) {
            return nodeMap.get(node);
        }
        return null;
    }

    public LevelNode getNode(LevelNode levelNode) {
        return getNode(levelNode.getExternalId());
    }

    public Long getCurrentNodeId() {
        return currentNodeId;
    }

    private void setCurrentNodeId() {
        this.currentNodeId++;
    }

    private Long getAndIncrementNodeId() {
        return this.currentNodeId++;
    }

    public Long getCurrentEdgeId() {
        return currentEdgeId;
    }

    private void setCurrentEdgeId(Long currentEdgeId) {
        this.currentEdgeId = currentEdgeId;
    }

    private boolean removeEdgeInternal(Long from, Long to, Long deletingTransaction) {
        if (nodeMap.containsKey(from) && cachedEdges.containsKey(nodeMap.get(from))) {
            LevelNode levelNode = nodeMap.get(from);
            int id = cachedEdges.get(levelNode).indexOf(new Edge(from, to, (long) -1, '2', null)); //direction bug TODO
            cachedEdges.get(levelNode).get(id).setDeletingTransaction(deletingTransaction);
            return true;
        }
        return false;
    }

    public Long addEdge(Long nodeId, Edge edge) {
        cachedEdges.putIfAbsent(nodeMap.get(nodeId), new ArrayList<Edge>()); //What if there is a duplicate edge? Use bloom filter?
        edge.setEdgeid(currentEdgeId++);
        cachedEdges.get(nodeMap.get(nodeId)).add(edge);
        return edge.getEdgeid();
    }


    @Override
    public ArrayList<Properties> getProperties(LevelNode levelNode) {
        return nodePropertyStore.get(levelNode);
    }

    @Override
    public ArrayList<Properties> getProperties(Edge edge) {
        return edgePropertyStore.get(edge);
    }


    // This operation removes any edge that has not yet been flushed to disk.
    // If deletion fails, it is upto the storage to handle it.

    public boolean deleteEdge(Long from, Long to, Long deletingTransaction) {
        //Delete paired edge
        return removeEdgeInternal(from, to, deletingTransaction) || removeEdgeInternal(to, from, deletingTransaction);
    }

    public void addNode(Long node) {
        LevelNode internalLevelNode = new LevelNode(node);
        cachedEdges.put(internalLevelNode, new ArrayList<Edge>());
        setCurrentNodeId();
        internalLevelNode.setInternalId(getCurrentNodeId());
        nodeMap.put(node, internalLevelNode);
        prevData.put(internalLevelNode, new Pair<Integer, Integer>(currentBlock, 0));
    }

    public Long addNode(LevelNode levelNode) {
        Long nodeId = getAndIncrementNodeId();
//        setCurrentNodeId();
        levelNode.setInternalId(nodeId);
        cachedEdges.put(levelNode, new ArrayList<Edge>());
        nodeMap.put(nodeId, levelNode);
        prevData.put(levelNode, new Pair<Integer, Integer>(currentBlock, 0));
        return nodeId;
    }

    public ArrayList<Edge> getEdges(Long nodeId) {
        LevelNode levelNode = new LevelNode(nodeId);
        if (cachedEdges.containsKey(levelNode)) {
            return cachedEdges.get(levelNode);
        }
        return null;
    }

    @Override
    public ArrayList<Edge> getEdges(LevelNode levelNode, long creatingTransaction) {
        if (nodeMap.containsKey(levelNode.getInternalId())) {
            return getEdges(levelNode.getExternalId());
        }
        return null;
    }

    @Override
    public void addProperty(LevelNode node, Properties properties) {
        nodePropertyStore.putIfAbsent(node, new ArrayList<>());
        nodePropertyStore.get(node).add(properties);
    }

    @Override
    public void addProperty(Edge edge, Properties properties) {
        edgePropertyStore.putIfAbsent(edge, new ArrayList<>());
        nodePropertyStore.get(edge).add(properties);
    }

    public Pair<Long, Long> getBulkNodeIds() {
        return new Pair<Long, Long>((long) 0, (long) 0);
    }
}

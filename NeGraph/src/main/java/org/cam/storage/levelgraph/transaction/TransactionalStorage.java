/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.transaction;

import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.datatypes.StorableData;
import org.cam.storage.levelgraph.results.MultiEdgeResult;
import org.cam.storage.levelgraph.storage.UnifiedDataStorage;
import org.cam.storage.levelgraph.storage.dataqueue.WriteAheadLog;

import java.util.ArrayList;

import static org.cam.storage.levelgraph.transaction.UpdateTarget.EdgeUpdate;
import static org.cam.storage.levelgraph.transaction.UpdateType.Create;

/**
 *
 */
public class TransactionalStorage implements TransactionStorageInterface {

    TransactionUpdatesCache transactionUpdatesCache;

    TransactionsResultsCache transactionsResultsCache;

    UnifiedDataStorage storage;

    TransactionInterface transaction;

    WriteAheadLog wal;


    public TransactionalStorage(TransactionInterface transaction) {
        transactionUpdatesCache = new TransactionUpdatesCache();
        transactionsResultsCache = new TransactionsResultsCache();
    }

    public TransactionalStorage(UnifiedDataStorage storage, WriteAheadLog wal) {
        transactionUpdatesCache = new TransactionUpdatesCache();
        transactionsResultsCache = new TransactionsResultsCache();
        this.storage = storage;
        this.wal = wal;
    }

    public void setStorage(UnifiedDataStorage storage) {
        this.storage = storage;
    }

    public void setWal(WriteAheadLog wal) {
        this.wal = wal;
    }

    @Override
    public Long addEdge(Edge edge) {
        transactionUpdatesCache.addUpdate(Create, EdgeUpdate, edge);
        transactionsResultsCache.addResult(edge);
        return (long) 0;
    }

    @Override
    public Long addNode(LevelNode levelNode) {
        transactionUpdatesCache.addUpdate(Create, UpdateTarget.NodeUpdate, levelNode);
        transactionsResultsCache.addResult(levelNode);
        return (long) 0;
    }


    //Need to add a conditional read edge etc.
    @Override
    public StorableData getEdges(LevelNode levelNode) {
        transactionsResultsCache.addResult(new MultiEdgeResult(storage.getEdges(levelNode, transaction.getTransactionId())));

        return null;
    }

    @Override
    public void deleteNode(LevelNode levelNode) {
        transactionUpdatesCache.addUpdate(UpdateType.Delete, UpdateTarget.NodeUpdate, levelNode);
    }

    @Override
    public LevelNode findNode(LevelNode levelNode) {

        LevelNode temp = storage.getNode(levelNode);
        if (temp != null) {
            levelNode = temp;
        } else {
            temp = (LevelNode) transactionUpdatesCache.getCreatedNodes().get(levelNode.getInternalId());
            if (temp != null)
                levelNode = temp;
//            temp=transactionUpdatesCache.;
        }
        return levelNode;

//TODO
    }

    @Override
    public void deleteEdge(Edge edge) {
        transactionUpdatesCache.addUpdate(UpdateType.Delete, EdgeUpdate, edge);
    }

    @Override
    public Long getTransactionId() {
        return transaction.getTransactionId();
    }

    public ArrayList<StorableData> getResults() {
        return transactionsResultsCache.getResults();
    }

    public void setTransaction(TransactionInterface transaction) {
        this.transaction = transaction;
    }

    public void commit() {

        ArrayList<TransactionUpdatesCache.Updates> adds = transactionUpdatesCache.getAdditionList();

        ArrayList<TransactionUpdatesCache.Updates> deletes = transactionUpdatesCache.getDeletionList();

        for (TransactionUpdatesCache.Updates update : adds) {
            //Addition and deletions only handled here, TODO handle updates
            StorableData data = update.getData();
            if (data instanceof LevelNode) {
                wal.addNode((LevelNode) data);
                storage.addNode((LevelNode) data);
            }
            if (data instanceof Edge) {
                wal.addEdge((Edge) data);
                storage.addEdge((Edge) update.getData());
            }
        }
        for (TransactionUpdatesCache.Updates update : deletes) {
            StorableData data = update.getData();
            if (data instanceof LevelNode) {
                wal.deleteNode((LevelNode) data);
                storage.deleteNode((LevelNode) data, getTransactionId());
            }
            if (data instanceof Edge) {
                wal.deleteEdge((Edge) data);
                storage.deleteEdge((Edge) data, getTransactionId());
            }
        }
        transactionUpdatesCache.clearCache();
    }

}

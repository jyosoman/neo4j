/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.datatypes;

import org.cam.storage.levelgraph.dataUtils.PrimitiveDeserialiser;
import org.cam.storage.levelgraph.dataUtils.PrimitiveSerialiser;

public class LevelNode implements StorableData {

    private Long internalId;

    private Long creatingTransaction;

    private Long deletingTransaction;

    //External Id is a temporary representation used for a string identifier for a levelNode.

    private Long externalId;


    public LevelNode(long externalId) {
        this.externalId=externalId;
        internalId=(long)-1;
        creatingTransaction=(long)-1;
        deletingTransaction=(long)-1;
    }

    public LevelNode(byte[] bytes) {
        internalId = PrimitiveDeserialiser.getInstance().longFromByteArray(bytes, 1);
        externalId = PrimitiveDeserialiser.getInstance().longFromByteArray(bytes, 9); //No need to deserialise externalId, it should not be stored in the first place on disk.
    }
    @Override
    public byte[] toBytes() {
        byte[] results;
        byte[] id = PrimitiveSerialiser.getInstance().longToBytes(internalId);
        byte[] eid = PrimitiveSerialiser.getInstance().longToBytes(externalId);
        results = new byte[1 + id.length + eid.length];
        int index = 0;
        results[0] = (byte) 17;
        index += 1;
        System.arraycopy(id, 0, results, index, id.length);
        index += id.length;
        System.arraycopy(eid, 0, results, index, eid.length);
        return results;
    }

    public Long getExternalId() {
        return externalId;
    }

    public Long getCreatingTransaction() {
        return creatingTransaction;
    }

    public void setCreatingTransaction(Long creatingTransaction) {
        this.creatingTransaction = creatingTransaction;
    }

    public Long getDeletingTransaction() {
        return deletingTransaction;
    }

    public void setDeletingTransaction(Long deletingTransaction) {
        this.deletingTransaction = deletingTransaction;
    }

    public int hashCode(){
        return internalId.hashCode();
    }

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }

    public boolean equals(Object other) {
        if (other instanceof LevelNode) {
            return internalId == ((LevelNode) other).internalId;
        }
        return false;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public String toString(){
        return " "+externalId+" ";
    }

}

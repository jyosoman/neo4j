/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.transaction;

import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.datatypes.StorableData;

import java.util.ArrayList;
import java.util.HashMap;

/*
    Transaction cache which holds the individual updates that need to be made to the storage by the transaction.
    Results cache for the transaction is separate from this. That holds the results to be sent upstream.
 */
public class TransactionUpdatesCache {
    private ArrayList<Updates> additionList, deletionList;

    private HashMap<Long, StorableData> createNode, createEdge;

    public TransactionUpdatesCache() {
        additionList = new ArrayList<>();
        deletionList = new ArrayList<>();
        createEdge = new HashMap<>();
        createNode = new HashMap<>();
    }

    public void clearCache() {
        additionList.clear();
        deletionList.clear();
        createEdge.clear();
        createNode.clear();
    }

    void addUpdate(UpdateType updateType, UpdateTarget target, StorableData data) {
        Updates updates = new Updates(updateType, target, data);
        switch (updateType) {
            case Update:
                additionList.add(updates);
                break;
            case Delete:
                deletionList.add(updates);
                break;
            case Create:
                additionList.add(updates);
                if (target == UpdateTarget.EdgeUpdate) {
                    createEdge.put(((Edge) data).getEdgeid(), data);
                } else {
                    if (target == UpdateTarget.NodeUpdate) {
                        createNode.put(((LevelNode) data).getInternalId(), data);
                    }
                }
                break;
            default:
                additionList.add(updates);
        }
    }

    public HashMap<Long, StorableData> getCreatedNodes() {
        return createNode;
    }

    public HashMap<Long, StorableData> getCreatedEdges() {
        return createEdge;
    }

    //Having additions and deletions separately would make life easier.
    public class Updates{
        UpdateType updateType;
        UpdateTarget updateTarget;
        StorableData data;

        public UpdateType getUpdateType() {
            return updateType;
        }

        public UpdateTarget getUpdateTarget() {
            return updateTarget;
        }

        public StorableData getData() {
            return data;
        }

        private void getUpdateTypesFromQuery(String query){
            updateType = UpdateType.Create;
            updateTarget=UpdateTarget.EdgeUpdate;
        }

        Updates(UpdateType updateType, UpdateTarget updateTarget,StorableData storableData){ //TODO Convert result to something meaningful and query to a deserialised format
            this.updateType=updateType;
            this.updateTarget=updateTarget;
            data=storableData;
        }

        byte[] storableUpdate() {
            byte[] dataInBytes = data.toBytes();
            byte[] result = new byte[2 + dataInBytes.length];
            result[0] = updateType.toByte(updateType);
            result[1] = updateTarget.toByte(updateTarget);
            System.arraycopy(dataInBytes, 0, result, 2, dataInBytes.length);
            return result;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Updates)) return false;

            Updates updates = (Updates) o;

            return data.equals(updates.data);
        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }
    }

    public ArrayList<Updates> getDeletionList() {
        return deletionList;
    }

    public ArrayList<Updates> getAdditionList() {
        return additionList;
    }

}

/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */
package org.neo4j.support.versioning.date;

import org.neo4j.graphdb.*;

import java.util.Iterator;
import java.util.Map;

public class VersionedNode implements Node
{
    private Node node;
    private VersionContext versionContext;

    public VersionedNode( Node node, VersionContext versionContext )
    {
        this.node = node;
        this.versionContext = versionContext;
    }

    public long getId()
    {
        return node.getId();
    }

    public void delete()
    {
        versionContext.deleteNode( node );
    }

    public ResourceIterable<Relationship> getRelationships()
    {
        return getValidRelationships( node.getRelationships() );
    }

    public ResourceIterable<Relationship> getRelationships( Direction dir )
    {
        return getValidRelationships( node.getRelationships( dir ) );
    }

    public ResourceIterable<Relationship> getRelationships( RelationshipType... types )
    {
        return getValidRelationships( node.getRelationships( types ) );
    }

    public ResourceIterable<Relationship> getRelationships( RelationshipType type, Direction dir )
    {
        return getValidRelationships( node.getRelationships( type, dir ) );
    }

    @Override
    public ResourceIterable<Relationship> getRelationships( Direction direction, RelationshipType... types )
    {
        return getValidRelationships( node.getRelationships( direction, types ) );
    }


    private ResourceIterable<Relationship> getValidRelationships(Iterable<Relationship> relations){
        return (ResourceIterable<Relationship>) relations;
    }
    public boolean hasRelationship()
    {
        return getRelationships().iterator().hasNext();
    }

    public boolean hasRelationship( Direction dir )
    {
        return getRelationships( dir ).iterator().hasNext();
    }

    public boolean hasRelationship( RelationshipType... types )
    {
        return getRelationships( types ).iterator().hasNext();
    }

    public boolean hasRelationship( RelationshipType type, Direction dir )
    {
        return getRelationships( type, dir ).iterator().hasNext();
    }

    @Override
    public boolean hasRelationship( Direction direction, RelationshipType... types )
    {
        return getRelationships(direction, types ).iterator().hasNext();
    }

    public Relationship getSingleRelationship( RelationshipType type, Direction dir )
    {
        Iterator<Relationship> iter = getRelationships( type, dir ).iterator();
        if ( !iter.hasNext() )
        {
            return null;
        }
        Relationship single = iter.next();
        if ( iter.hasNext() ) throw new NotFoundException( "More than one relationship[" +
            type + ", " + dir + "] found for " + this + " in " + versionContext );
        return single;
    }

    public Relationship createRelationshipTo( Node otherNode, RelationshipType type )
    {
        return new VersionedRelationship( node.createRelationshipTo( otherNode, type ), versionContext );
    }


    public GraphDatabaseService getGraphDatabase()
    {
        return node.getGraphDatabase();
    }

    public boolean hasProperty( String key )
    {
        return versionContext.hasProperty( node, key );
    }

    public Object getProperty( String key )
    {
        return versionContext.getProperty( node, key );
    }

    public Object getProperty( String key, Object defaultValue )
    {
        return versionContext.getProperty( node, key, defaultValue );
    }

    public void setProperty( String key, Object value )
    {
        node.setProperty( key, value );
    }

    public Object removeProperty( String key )
    {
        return node.removeProperty( key );
    }

    public Iterable<String> getPropertyKeys()
    {
        return null;//versionContext.getPropertyKeys( levelNode );
    }

    public Iterable<Object> getPropertyValues()
    {
        return null;
        //return versionContext.getPropertyValues( levelNode );
    }

    @Override
    public int hashCode()
    {
        return node.hashCode();
    }

    @Override
    public boolean equals( Object obj )
    {
        return node.equals( obj );
    }

    public Iterable<RelationshipType> getRelationshipTypes(){
        return null;
    }
    @Override
    public int getDegree() {
        return 0;
    }

    @Override
    public int getDegree(RelationshipType relationshipType) {
        return 0;
    }

    @Override
    public int getDegree(Direction direction) {
        return 0;
    }

    public int getDegree(RelationshipType tp, Direction dir){
        return 0;
    }

    public void removeLabel(Label var){

    }
    @Override
    public Iterable<Label> getLabels() {
        return null;
    }

    @Override
    public boolean hasLabel(Label label) {
        return false;
    }

    @Override
    public Map<String, Object> getAllProperties() {
        return null;
    }

    @Override
    public void addLabel(Label label) {

    }

    @Override
    public Map<String, Object> getProperties(String... strings) {
        return null;
    }
}

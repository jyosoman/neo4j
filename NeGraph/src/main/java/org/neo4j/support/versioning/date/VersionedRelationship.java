/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */
package org.neo4j.support.versioning.date;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

import java.util.Map;

public class VersionedRelationship implements Relationship
{
    private Relationship relationship;
    private VersionContext versionContext;

    public VersionedRelationship( Relationship relationship, VersionContext versionContext )
    {
        this.relationship = relationship;
        this.versionContext = versionContext;
    }

    public long getId()
    {
        return relationship.getId();
    }

    public void delete()
    {
        versionContext.deleteRelationship( relationship );
    }

    public Node getStartNode()
    {
        return new VersionedNode( relationship.getStartNode(), versionContext );
    }

    public Node getEndNode()
    {
        return new VersionedNode( relationship.getEndNode(), versionContext );
    }

    public Node getOtherNode( Node node )
    {
        return new VersionedNode( relationship.getOtherNode( node ), versionContext );
    }

    public Node[] getNodes()
    {
        Node[] nodes = relationship.getNodes();
        return new Node[] { new VersionedNode( nodes[0], versionContext ), new VersionedNode( nodes[1], versionContext ) };
    }

    public RelationshipType getType()
    {
        return relationship.getType();
    }

    public boolean isType( RelationshipType type )
    {
        return relationship.isType( type );
    }

    public GraphDatabaseService getGraphDatabase()
    {
        return relationship.getGraphDatabase();
    }

    public boolean hasProperty( String key )
    {
        return relationship.hasProperty( key );
    }

    public Object getProperty( String key )
    {
        return relationship.getProperty( key );
    }

    public Object getProperty( String key, Object defaultValue )
    {
        return relationship.getProperty( key, defaultValue );
    }

    public void setProperty( String key, Object value )
    {
        relationship.setProperty( key, value );
    }

    public Object removeProperty( String key )
    {
        return relationship.removeProperty( key );
    }

    public Iterable<String> getPropertyKeys()
    {
        return relationship.getPropertyKeys();
    }

    @Override
    public Map<String, Object> getProperties(String... strings) {
        return null;
    }

    @Override
    public Map<String, Object> getAllProperties() {
        return null;
    }

    public Iterable<Object> getPropertyValues()
    {
        return null;//relationship.getPropertyValues();
    }

    @Override
    public int hashCode()
    {
        return relationship.hashCode();
    }

    @Override
    public boolean equals( Object obj )
    {
        return relationship.equals( obj );
    }
}
